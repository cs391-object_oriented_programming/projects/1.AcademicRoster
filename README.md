Date: 20161013

Develop an object oriented college student management system. You should first
implement a CollegeStudent class with appropriate data members such as Name, Year,
ExpectedGraduationYear, Major, Minor, GPA, etc... The class should have at least 6
methods in its public section. For example, there should be a method to compute GPA
and another method to determine whether the GPA achieves the Deans list or Probation.
Also, you MUST use at least one constructor and one destructor.