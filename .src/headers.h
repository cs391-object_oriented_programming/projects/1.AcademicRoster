#ifndef SOMETHING_H
#define SOMETHING_H
#include <iostream>
using namespace std;

/*SHORTEN COUT REPETITION*/
//Number, Char to repeat, Optional post-repetition char
static string r(int nr,char rep,char end='j') {
	if(end=='j') return string(nr,rep);
	else return string(nr,rep)+string(1,end);
}

struct studentinfo {
	int id;
	string surname;
	string name;
	string year;
	int taken;
	int grad;
	string major;
	string minor;
	studentinfo(int, string, string, string, int, int, string, string);
};

class CollegeStudent {
	public:
		CollegeStudent() :selected(0), prob(1,0.0){}				
		~CollegeStudent() {
			cout << "\n\n\t\t Program finished...Goodbye!\n\t";
		}
		void setFiles(const string&,const string&,const string&);
		void executor(int);
		void freeBird() { //free up Pointers variables
			id=NULL;
			surname=NULL;
			name=NULL;
			yr=NULL;
			grad=NULL;
			major=NULL;
			minor=NULL;
		}		
		void dean();
		vector<double> prob;
	private:
		double calcGPA();
		void comboClasses(int);
		void fullRoster();
		void detailedInfo();
		vector<studentinfo> vstuds();
		vector<vector<string> > vvCnames();
		vector<vector<double> > vvCgrades();
		vector<string> letterGrades(vector<double>);
		int selected; //selected student number
		string rosterfile;
		string classesfile;
		string gradesfile;
		
		int *id;
		string *surname;
		string *name;
		string *yr;
		int *grad;
		string *major;
		string *minor;
		double gpa;
};
#endif