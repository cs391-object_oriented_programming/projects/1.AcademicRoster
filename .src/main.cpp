/*********************************************************************
*   Name: Jose Madureira	CS391-Fall 2016			                 *
*	
*	Date: 18 October 2016							  				 *
*	Project: Student Management System								 *
*													  				 *
*													  				 *
*********************************************************************/
#include <iostream>
#include <vector>
#include <string>
#include <limits>//test cin for valid input
#include "headers.h"
using namespace std;

int main() {
	cout <<"\t..........................................\n"
	     <<"\t###     Student Academic Database        ###\n"
	     <<"\t..........................................\n";

	string rosterF= "studentinfo.csv";
	string classesF="classes.csv";
	string gradesF="grades.csv";

	CollegeStudent *cs = new CollegeStudent();
	cs->setFiles(rosterF,classesF,gradesF);
	
	studentinfo *si = new studentinfo(0,"","","",0,0,"","");
	
	int ch;
	do {
		cout<<"\n\t\t   .:Main Menu:.";
		cout<<"\n\t//   (1) View Student Roster "<<"\n\t//   (2) View Detailed Student Info"
		    <<"\n\t\\\\   (3) Quit\n";		    
		do  {
			cout<<">>You Choose: ";
			while(!(cin >> ch)) {
				cin.clear();
				cin.ignore(numeric_limits<streamsize>::max(), '\n');
				cout << "Invalid input.  Try again: ";
			}
		} while(ch<1 || ch>3);
		cout<<endl;
		cs->executor(ch);	//choice handler
	} while(ch!=3);
	
	delete si;
	delete cs;
	return 0;
}