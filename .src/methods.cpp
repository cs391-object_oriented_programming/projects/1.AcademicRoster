#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <iterator>
#include <iomanip>
#include <math.h>
#include <limits>
#include <sstream>
#include "headers.h"
using namespace std;

studentinfo::studentinfo(int i, string sn, string nm, string yr, int tkn, int g, string ma
                         ,string mi)
	:id(i),surname(move(sn)),name(move(nm)),year(move(yr)),taken(move(tkn)), grad(move(g))
	,major(move(ma)),minor(move(mi)) {};

void CollegeStudent::setFiles(const string& roster,const string& classes,const string& grades) {
	rosterfile=roster;
	classesfile=classes;
	gradesfile=grades;
}

void CollegeStudent::executor(int ch) {
	switch (ch) {
		case 1:
			system("cls"); //clear screen
			fullRoster();
			break;
		case 2:
			detailedInfo();
			break;
		case 3:
			system("cls");
			cin.get();
			break;
		default:
			cout<<"\n\t\t/!\\Invalid Menu Selection/!\\\n";
	}
};

void CollegeStudent::dean() {
	/*Credits Attempted	Minimum Cumulative GPA (Index)
	0 - 12	1.50	0-4 class MAX
	13 - 24	1.75	5-8 classes MAX
	25+	2.00		9+ classes MAX */
	
	int sz= prob.size();
	if(sz<=0) {
		cout<<"\nERROR, Received Student with Zero Classes";
		return;
	}
	
	double sum=0, test;
	for(auto it: prob) {
		sum+=it;
	}
	test= sum/sz;	//regular gpa

	bool pk=0;
	if(sz<=4 && (test<1.5)) pk=1;
	if((sz>4 && sz<=8) && (test<1.75)) pk=1;
	if(sz>=9 && test<2) pk=1;

	cout<<" Probation Status:  ";
	if(pk==1) cout<<"Probation.\n";
	else cout<<"None.\n";
}

void CollegeStudent::fullRoster() {
	char d='-',p='|',s=' ';
	string sr=".:Student Roster:.";
	cout<<r(19,s)<<r(sr.length(),'#')<<endl
	    <<r(19,s)<<sr<<endl
	    <<r(19,s)<<r(sr.length(),'#')<<endl;

	cout<<r(9,s)<<"ID     |Last Name  |First Name  | Year"<<endl;
	cout<<r(54,d)<<endl;

// #Display all Students
	vector<studentinfo> vsi= move(vstuds());
	int ktr= 0;
	for(auto it: vsi) {
		++ktr;
		printf(" (%0-2d) %-8d  |%-9s  |%-10s  |%-9s  |\n",ktr,it.id,(it.surname).c_str()
		       ,(it.name).c_str(),(it.year).c_str());
	}
	cout<<r(54,d)<<endl;
}

void CollegeStudent::detailedInfo() {
// #Get Number of Classes in File
	int rcount=0;
	ifstream ifst(classesfile);
	string junk;
	while (getline(ifst,junk))
		++rcount;
	ifst.close();

// #Ensure Selected Number is Valid#
	int stud=0;
	while(!(stud>=1) || stud>rcount) {
		cout<<"Enter a row between 1 and "<<rcount<<": ";
		cin>>stud;
		while(cin.fail()) {
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
			cout << "\nInvalid input.  Try again: ";
			cin>>stud;
		}
	}
	cout<<endl;
	selected= stud;
	--stud; //index 1 to 0
// #Detailed Sudent Info
	vector<studentinfo> vsi= move(vstuds());	//full student roster
	id= &vsi.at(stud).id;
	surname= &vsi.at(stud).surname;
	name= &vsi.at(stud).name;
	yr= &vsi.at(stud).year;
	grad= &vsi.at(stud).grad;
	major= &vsi.at(stud).major;
	minor= &vsi.at(stud).minor;

	int tkn= vsi.at(stud).taken;

	char d='-',p='|',s=' ';
// #General Info
	cout<<r(71,d)<<endl;
	cout<<r(6,s)<<"ID   | Last Name | First Name |    Year   | Grad\n";
	cout<<r(71,d)<<endl;
	printf("  %-8d | %-9s | %-10s | %-9s | %-4d\n",(*id),(*surname).c_str(),(*name).c_str()
	       ,(*yr).c_str(),*grad);

// #Major & Minor
	cout<<r(71,d)<<endl;
	printf("  Major: %-36s \n  Minor: %-22s\n",(*major).c_str(),(*minor).c_str());
	cin.get();

// #Display Classes and Grades
	comboClasses(stud);

// #Display GPA
	cout<<r(71,s)<<endl;
	gpa= calcGPA();
	if (tkn==0) printf("New Student\n");
	else cout<<"  Classes Taken: "<<tkn<<"  |  GPA: "<<gpa<<"   | ";

// #Display Probation Status
	if(tkn>0) dean();
	cout<<r(71,d)<<endl;

	freeBird(); //nullify pointers
}

double CollegeStudent::calcGPA() {
	vector<vector<double> > vgpa;
	vgpa= move(vvCgrades());

	int szRows= vgpa.size();
	int xrow= selected; //stored in class
	int szCol= vgpa[xrow-1].size();
	double junk;
	double sum=0.0;

	/*Store Grades for Probation Check*/
	prob= vgpa[xrow-1];

	for(auto it : vgpa[xrow-1]) {
		junk=it; //remove ID col
		sum+=it;
	}

	return sum/szCol;
}

vector<studentinfo> CollegeStudent::vstuds() {
//remove header row junk -  Adam @ http://stackoverflow.com/a/164367/6793751
	vector<studentinfo> vTmp;
	fstream f(rosterfile);

	string junk;
	getline(f,junk,'\n'); //skip header row

	string line=" ";
	while(getline(f,line,'\n')) {
		studentinfo *si= new studentinfo(0,"","","",0,0,"","");
		vector<string> vstest;
		stringstream ss(line);

		string in_line;
		int cktr=0; //cols
		while(getline (ss, in_line, ',')) {
			vstest.push_back(in_line);
			if(cktr==0) (*si).id= 		stoi(in_line);
			if(cktr==1) (*si).surname= in_line;
			if(cktr==2) (*si).name= 	in_line;
			if(cktr==3) (*si).year= 	in_line;
			if(cktr==4) (*si).taken= 	stoi(in_line);
			if(cktr==5) (*si).grad= 	stoi(in_line);
			if(cktr==6) (*si).major= 	in_line;
			if(cktr==7) (*si).minor= 	in_line;
			++cktr;
		}
		vTmp.emplace_back(*si);
		delete si;
	}
	f.close();
	return vTmp;
}

vector<string> CollegeStudent::letterGrades(vector<double> l2g) {
	vector<double> vl;
	vl= move(l2g);
	vector<string> tmp;

	int sz=l2g.size();
	string s;
	for(auto it=vl.begin(); it<vl.end(); ++it) {
		if(*it==0) s="";
		else if(*it>0 && *it<1) s="F";
		else if(*it>=1 && *it<1.3) s="D";
		else if(*it>=1.3 && *it<1.7) s="D+";
		else if(*it>=1.7 && *it<2.0) s="C-";
		else if(*it>=2.0 && *it<2.3) s="C";
		else if(*it>=2.3 && *it<2.7) s="C+";
		else if(*it>=2.7 && *it<3.0) s="B-";
		else if(*it>=3.0 && *it<3.3) s="B";
		else if(*it>=3.3 && *it<3.7) s="B+";
		else if(*it>=3.7 && *it<4.0) s="A-";
		else if(*it==4.0) s="A";
		else if(*it>4.0 && *it<=4.33) s="A+";
		else {
			cout<<"\n\tINVALID GRADE\n";
			break;
		}
		tmp.emplace_back(s);
	}
	return tmp;
}

void CollegeStudent::comboClasses(int xin) {
	/*### Class Grades ###*/
	vector<vector<double> > vvg;
	vvg= vvCgrades();

	vector<double> vg= move(vvg[xin]);
	vector<string> vlg;
	vlg= letterGrades(vg); //grade point to letter

	/*### Class Names ###*/
	vector<vector<string> > vvn;
	vvn= vvCnames();
	vector<string> vn= move(vvn[xin]);

	int sz= vn.size();
	for(int a=0; a<sz; ++a) {
		if((a%5)==0) printf("\n%s\n|",string(71,'-').c_str());
		printf("%-*s: %-*s|",8,vn[a].c_str(),3,vlg[a].c_str());
		if(a==(sz-1)) printf("\n%s\n",string(71,'-').c_str());
	}
}

/*#### READ CLASS NAMES FILE ###*/
vector<vector<string> > CollegeStudent::vvCnames() {
	vector<vector<string> > vvc;

	string line;
	ifstream ifs(classesfile);
	if (!ifs) cerr << "\n\n\t\t/!\\File '"<<classesfile<<"' failed to open\n\n";
	else {
		while(getline(ifs, line, '\n')) {
			stringstream ss(line);
			vector<string> vc;
			string in_line;
			int ktr=0;
			while(getline (ss, in_line, ',')) {
				if(ktr!=0)	//skip id row
					vc.emplace_back(in_line);
				++ktr;
			}
			vvc.push_back(vc);
		}
	}

	ifs.close();
	return vvc;
}

/*#### READ CLASS GRADES FILE ###*/
vector<vector<double> > CollegeStudent::vvCgrades() {
//File to vector http://stackoverflow.com/a/23072197/6793751
	vector<vector<double> > vvg;

	string line;
	ifstream ifs(gradesfile);
	if (!ifs) cerr << "\n\n\t\t/!\\File '"<<gradesfile<<"' failed to open\n\n";
	else {
		while(getline(ifs, line, '\n')) {
			stringstream ss(line);
			vector<double> vg;
			string in_line;
			int ktr=0;
			while(getline (ss, in_line, ',')) {
				double g;
				if (ktr!=0) //skip id row
					vg.emplace_back(stod(in_line));
				++ktr;
			}
			vvg.push_back(vg);
		}
	}

	ifs.close();
	return vvg;
}